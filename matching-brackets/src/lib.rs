use std::collections::HashMap;

pub fn brackets_are_balanced(string: &str) -> bool {
    //I prefer this instead of using insert a bunch of times
    let brackets: HashMap<char, char> = [('(', ')'), ('{', '}'), ('[', ']')]
        .iter()
        .cloned()
        .collect();
    //Stack that will keep track of tokens
    let mut stack: Vec<char> = vec![];
    //Opening and closing tokens
    let opening: &str = "{([";
    let closing: &str = "})]";

    for token in string.chars() {
        //if opening token, insert into stack
        if opening.contains(token) {
            stack.push(token)
        } else if closing.contains(token) {
            match stack.last() {
                Some(opening_token) => {
                    //is opening and closing token correspond to each other we can remove closing token, else we have no opening got current token hence return false
                    if brackets.get(opening_token).unwrap() == &token {
                        stack.pop();
                    } else {
                        return false;
                    }
                }
                //If stack has no element
                None => return false,
            }
        }
    }
    //Stack should be empty, count of opening tokens == closing tokens
    stack.len() == 0
}
