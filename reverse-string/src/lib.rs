use unicode_segmentation::UnicodeSegmentation;

pub fn reverse(input: &str) -> String {
    UnicodeSegmentation::graphemes(input, true).rev().collect()
    //@@ Ignoring grapheme clusters this will work perfectly well
    //input.chars().rev().collect()
}
