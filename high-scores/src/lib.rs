#[derive(Debug)]
pub struct HighScores<'a> {
    scores: &'a [u32],
}

impl<'a> HighScores<'a> {
    pub fn new(scores: &'a [u32]) -> Self {
        Self { scores: scores }
    }

    pub fn scores(&self) -> &[u32] {
        self.scores
    }

    pub fn latest(&self) -> Option<u32> {
        match self.scores.last() {
            Some(s) => Some(*s),
            None => None,
        }
    }

    pub fn personal_best(&self) -> Option<u32> {
        self.scores.into_iter().max().copied()
    }

    pub fn personal_top_three(&self) -> Vec<u32> {
        let mut scores = self.scores().to_vec();
        scores.sort();
        scores
            .into_iter()
            .rev()
            .take(3)
            .clone()
            .collect::<Vec<u32>>()
    }
}
