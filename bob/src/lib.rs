pub fn reply(message: &str) -> &str {
    let trimmed_message = message.trim();

    let all_capital = |message: &str| {
        message
            .chars()
            .all(|character| !character.is_ascii_alphabetic() || character.is_ascii_uppercase())
            && message
                .chars()
                .any(|character| character.is_ascii_alphabetic())
    };
    if trimmed_message == String::new() {
        "Fine. Be that way!"
    } else if trimmed_message.chars().last().unwrap() == '?' {
        if all_capital(trimmed_message) {
            "Calm down, I know what I'm doing!"
        } else {
            "Sure."
        }
    } else if all_capital(trimmed_message) {
        "Whoa, chill out!"
    } else {
        "Whatever."
    }
}
