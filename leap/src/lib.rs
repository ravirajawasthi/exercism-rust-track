pub fn is_leap_year(year: u64) -> bool {
    let mut result: bool = false;
    if year % 4 == 0 {
        let evenly_divisible_100 = (year % 100) == 0 && (year % 100) % 2 == 0;
        let evenly_divisible_400 = (year % 400) == 0 && (year % 400) % 2 == 0;
        if !evenly_divisible_100 || evenly_divisible_400 {
            result = true;
        }
    }
    result
}
