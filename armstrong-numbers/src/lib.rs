pub fn is_armstrong_number(num: u32) -> bool {
    let length: usize = num.to_string().len();
    num.to_string()
        .chars()
        .map(|digit| digit.to_digit(10).unwrap().pow(length as u32))
        .sum::<u32>()
        == num
}
