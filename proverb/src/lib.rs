use std::iter::once;
pub fn build_proverb(list: &[&str]) -> String {
    //Old Solution
    // if list.len() == 0 {
    //     return String::new();
    // }
    // let mut list_iter = list.iter();
    // let mut result = String::new();
    // let sentence_generator =
    //     |word1: &str, word2: &str| format!("For want of a {} the {} was lost.", word1, word2);

    // let first = list_iter.next().unwrap();
    // let mut current = first;
    // for word in list_iter {
    //     result.push_str(sentence_generator(current, word).as_str());
    //     result.push_str("\n");
    //     current = word;
    // }
    // result.push_str(format!("And all for the want of a {}.", first).as_str());
    // result

    //Using iterators and closures
    if list.len() == 0 {
        return String::new();
    }
    list.windows(2)
        .map(|pair| format!("For want of a {} the {} was lost.\n", pair[0], pair[1]))
        .chain(once(format!("And all for the want of a {}.", list[0])))
        .collect()
}
