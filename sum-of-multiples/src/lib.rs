pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    (1..limit)
        .map(|num| {
            for factor in factors.iter() {
                if factor > &0 && num % factor == 0 {
                    println!("{}", num);
                    return num;
                }
            }
            0
        })
        .sum::<u32>()
}
