pub fn square_of_sum(n: u32) -> u32 {
    let sum = n as f64 * (n + 1) as f64 / 2.0;
    (sum * sum) as u32
}
pub fn sum_of_squares(n: u32) -> u32 {
    ((n + 1) as f64 * (2 * n + 1) as f64 * (n as f64 / 6.0)) as u32
}

pub fn difference(n: u32) -> u32 {
    square_of_sum(n) - sum_of_squares(n)
}
