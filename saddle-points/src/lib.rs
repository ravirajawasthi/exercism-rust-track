use std::cmp::min;
pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    //Calculating max in each rows
    let max_in_rows = match input.get(0) {
        Some(row) => {
            if row.len() > 0 {
                input.iter().map(|row| *row.iter().max().unwrap()).collect()
            } else {
                vec![]
            }
        }
        None => vec![],
    };

    //Calculating min in each column
    let min_in_columns: Vec<u64> = match input.get(0) {
        Some(row) => (0..row.len())
            .map(|j| (0..input.len()).fold(u64::MAX, |minimun, i| min(minimun, input[i][j])))
            .collect::<Vec<u64>>(),
        None => vec![],
    };

    let mut saddle_points: Vec<(usize, usize)> = Vec::new();
    for i in 0..input.len() {
        for j in 0..input[i].len() {
            if input[i][j] == max_in_rows[i] && input[i][j] == min_in_columns[j] {
                saddle_points.push((i, j));
            }
        }
    }
    saddle_points
}
