//#######################OLD SOLUTION########################################

// pub fn verse(n: u32) -> String {
//     let final_verse = || {
//         "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n"
//     };
//     let take = |num| -> String {
//         let mut verse = format!(
//             "Take {} down and pass it around, ",
//             if num == 1 { "it" } else { "one" },
//         );
//         if num == 1 {
//             verse.push_str("no more bottles of beer on the wall.\n")
//         } else {
//             verse.push_str(
//                 format!(
//                     "{} {} of beer on the wall.\n",
//                     num - 1,
//                     if num == 2 { "bottle" } else { "bottles" },
//                 )
//                 .as_str(),
//             )
//         }
//         verse
//     };
//     let bottle = |num: u32| -> String {
//         format!(
//             "{} {} of beer on the wall, {} {} of beer.\n",
//             num,
//             if num == 1 { "bottle" } else { "bottles" },
//             num,
//             if num == 1 { "bottle" } else { "bottles" }
//         )
//     };
//     if n == 0 {
//         String::from(final_verse())
//     } else {
//         String::from(bottle(n) + &take(n))
//     }
// }

// pub fn sing(start: u32, end: u32) -> String {
//     let mut result = String::new();
//     for n in (end..=start).rev() {
//         result.push_str(
//             format!(
//                 "{}{}",
//                 verse(n),
//                 if n == end {
//                     String::new()
//                 } else {
//                     String::from("\n")
//                 }
//             )
//             .as_str(),
//         )
//     }
//     result
// }
pub fn verse(n: u32) -> String {
    match n{
        0=> "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n".to_owned(),
        1=>"1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n".to_owned(),
        2=>"2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n".to_owned(),
        _=>format!("{} bottles of beer on the wall, {} bottles of beer.\nTake one down and pass it around, {} bottles of beer on the wall.\n",n,n,n-1)
    }
}

pub fn sing(start: u32, end: u32) -> String {
    let result: Vec<String> = (end..=start).rev().map(|v| verse(v)).collect();
    result.join("\n")
}
