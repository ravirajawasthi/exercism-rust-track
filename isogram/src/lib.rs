use std::collections::HashSet;

pub fn check(candidate: &str) -> bool {
    let mut is_isogram: bool = true;
    let mut set: HashSet<char> = HashSet::new();
    for character in candidate.chars() {
        if set.contains(&character.to_ascii_lowercase()) == false {
            set.insert(character.to_ascii_lowercase());
        } else if character != '-' && character != ' ' {
            is_isogram = false;
            break;
        }
    }
    is_isogram
}
