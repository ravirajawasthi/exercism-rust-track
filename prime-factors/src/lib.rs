pub fn factors(n: u64) -> Vec<u64> {
    println!("{}", n);
    if n <= 1 {
        vec![]
    } else {
        let divisible_factors = (2..=n).find(|factor| n % factor == 0).unwrap();
        let factor = divisible_factors;
        let recursion_result = &mut factors(n / factor);
        let mut x = vec![factor];
        x.append(recursion_result);
        x
    }
}
