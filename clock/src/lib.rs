use std::fmt;

#[derive(Debug, PartialEq)]
pub struct Clock {
    hours: u32,
    minutes: u32,
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        Clock {
            hours: 0,
            minutes: 0,
        }
        .add(hours, minutes)
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        self.add(0, minutes)
    }

    pub fn add(&self, hours: i32, minutes: i32) -> Self {
        //reduce m and h to minutes  ALWAYS POSITIVE
        let mut total_minutes = (self.hours * 60 + self.minutes) as i32;

        //given hours and minutes
        let given_minutes = hours * 60 + minutes;

        total_minutes += given_minutes;

        let total_minutes = if total_minutes.signum() < 0 {
            //total_minutes is negative, subtract minutes from a whole day
            (1440 - total_minutes.abs() % 1440) as u32
        } else {
            //Total_minutes is positive, nothing is required
            total_minutes.abs() as u32
        } % 1440; // % 1440 because we dont need extra days and 1440 is the amount of minutes in a day
        Self {
            hours: total_minutes / 60,
            minutes: total_minutes % 60,
        }
    }
}

impl fmt::Display for Clock {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:02}:{:02}", self.hours, self.minutes)
    }
}
