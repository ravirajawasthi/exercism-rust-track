pub fn nth(n: u32) -> u32 {
    let mut primes: Vec<u32> = vec![2];
    let mut num = 3;
    while primes.len() <= n as usize {
        let mut is_prime = true;
        for prime in &primes {
            if num % prime == 0 {
                is_prime = false;
                break;
            }
        }

        if is_prime {
            let last_prime = *primes.last().unwrap();
            for i in last_prime..((num as f32).sqrt() as u32) {
                if num % i == 0 {
                    is_prime = false;
                    break;
                }
            }

            if is_prime {
                primes.push(num);
            }
        }
        num += 1;
    }
    *primes.last().unwrap()
}
